FROM alpine:3.14.2
LABEL MAINTAINER Platform Engineering <platform@underwriteme.co.uk>

# Install necessary tooling
RUN apk update && apk upgrade
RUN apk update && apk add --no-cache \
  aws-cli \
  bash \
  bind-tools \
  busybox-extras \
  curl \
  iproute2 \
  grep \
  jq \
  mysql-client \
  netcat-openbsd \
  openssh-client \
  openssl \
  postgresql \
  rsync \
  strace \
  the_silver_searcher \
  tmux \
  yq \
  && apk upgrade -f apk-tools \
  && apk upgrade -f musl-utils \
  && rm -rf /var/cache/apk/*

# Install Python 3
RUN apk add --no-cache python3 && \
  if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
  python3 -m ensurepip && \
  rm -r /usr/lib/python*/ensurepip && \
  pip3 install --no-cache --upgrade pip setuptools wheel && \
  if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi

COPY ./rds-ca-bundle ./rds-ca-bundle

COPY ./scripts ./scripts

CMD ["/bin/sh"]
