# K8s Utils 🛠️

Minimal docker image for running useful tooling when debugging inside Kubernetes.

The container is scanned by [https://github.com/aquasecurity/trivy](https://github.com/aquasecurity/trivy) as part of CI.

# CVE justification

| Date       | Library          | Vulnerability ID | Severity  | Justification                                                                                                                       |
|------------|------------------|------------------|-----------|-------------------------------------------------------------------------------------------------------------------------------------|
| 06/03/2023 | libcom_err | CVE-2022-1304   | HIGH    | One of the libraries/ tools installed in the container, but we only use this container for debugging purposes, not day to day tasks |
| 02/03/2023 | golang.org/x/sys | CVE-2022-29526   | MEDIUM    | One of the libraries/ tools installed in the container, but we only use this container for debugging purposes, not day to day tasks |
| 02/03/2023 | gopkg.in/yaml.v3 | CVE-2022-28948   | HIGH      | One of the libraries/ tools installed in the container, but we only use this container for debugging purposes, not day to day tasks                                                                                                                                    |

