package trivy

import data.lib.trivy

default ignore = false

ignore_pkgs_golang := {"golang.org/x/sys","gopkg.in/yaml.v3"}

ignore_pkgs_libcom := {"libcom_err"}

ignore_vulnerability_ids_golang := {"CVE-2022-29526","CVE-2022-28948"}

ignore_vulnerability_ids_libcom := {"CVE-2022-1304"}

ignore {
    input.PkgName == ignore_pkgs_golang[_]
    input.VulnerabilityID == ignore_vulnerability_ids_golang[_]
}

ignore {
    input.PkgName == ignore_pkgs_libcom[_]
    input.VulnerabilityID == ignore_vulnerability_ids_libcom[_]
}