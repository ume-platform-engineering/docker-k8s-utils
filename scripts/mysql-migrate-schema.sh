#! /bin/bash
set -euo pipefail

check_env_var(){
    local env_var=${1}
    if [ -z "${!env_var}" ]
    then
        echo "required env var is not set: $env_var"
        exit 1
    fi
}

readonly schema_filename="source_schema.sql"

# check required env vars are set
required=(
    SOURCE_HOST
    SOURCE_USER
    SOURCE_PASS
    TARGET_HOST
    TARGET_USER
    TARGET_PASS
)
for env in ${required[@]}; do
  check_env_var $env
done

test_connection(){
    local db_host=${1}
    local db_user=${2}
    local db_pass=${3} 
    echo "testing connection to ${db_host}..."
    mysql -h"${db_host}" -u"${db_user}" -p"${db_pass}" -e "\q"
    echo "  -- success"
}

dump_schema(){
    local db_host=${1}
    local db_user=${2}
    local db_pass=${3}
    echo "dumping schema from '${db_host}' to ${schema_filename}"
    rm -f $schema_filename
    query="SET SESSION group_concat_max_len = 1000000;SELECT GROUP_CONCAT(SCHEMA_NAME SEPARATOR ' ') FROM information_schema.SCHEMATA WHERE SCHEMA_NAME NOT IN ('mysql','information_schema','performance_schema','sys','tmp','innodb');"
    databases=`mysql -h"${db_host}" -u"${db_user}" -p"${db_pass}" -Nse "${query}"`
    mysqldump --events --routines  --triggers --single-transaction --no-tablespaces --no-data --databases $databases -h"${db_host}" -u"${db_user}" -p"${db_pass}" > ${schema_filename}
    sed -i -e 's/DEFINER=`.*`@`%`//g' ${schema_filename}
}

import_schema(){
    local db_host=${1}
    local db_user=${2}
    local db_pass=${3}
    echo "importing schema to '${db_host}'"
    mysql -h"${db_host}" -u"${db_user}" -p"${db_pass}" < ${schema_filename}
}

# check connection to source and target
test_connection $SOURCE_HOST $SOURCE_USER $SOURCE_PASS
test_connection $TARGET_HOST $TARGET_USER $TARGET_PASS

# dump schema on source
dump_schema $SOURCE_HOST $SOURCE_USER $SOURCE_PASS

# import schema to target
import_schema $TARGET_HOST $TARGET_USER $TARGET_PASS

echo "done"