#! /bin/bash
set -euo pipefail

check_env_var(){
    local env_var=${1}
    if [ -z "${!env_var}" ]
    then
        echo "required env var is not set: $env_var"
        exit 1
    fi
}

# check required env vars are set
required=(
    SOURCE_HOST
    SOURCE_USER
    SOURCE_PASS
    TARGET_HOST
    TARGET_USER
    TARGET_PASS
)
for env in ${required[@]}; do
  check_env_var $env
done

test_connection(){
    local db_host=${1}
    local db_user=${2}
    local db_pass=${3} 
    echo "testing connection to ${db_host}..."
    mysql -h"${db_host}" -u"${db_user}" -p"${db_pass}" -e "\q"
    echo "  -- success"
}

# check connection to source and target
test_connection $SOURCE_HOST $SOURCE_USER $SOURCE_PASS
test_connection $TARGET_HOST $TARGET_USER $TARGET_PASS

# prepare source database
# set binlog retention
mysql -h"${SOURCE_HOST}" -u"${SOURCE_USER}" -p"${SOURCE_PASS}" -e "call mysql.rds_set_configuration('binlog retention hours', 168);"

# stop replication between source -> read replica
mysql -h"${SOURCE_HOST}" -u"${SOURCE_USER}" -p"${SOURCE_PASS}" -e "CALL mysql.rds_stop_replication;" || true

# setup replication on target
# get binlog coordinates
status=$(mysql -h"${SOURCE_HOST}" -u"${SOURCE_USER}" -p"${SOURCE_PASS}" -e "SHOW MASTER STATUS;")
binlog_file=$(echo $status | awk '{print $6}')
binlog_pos=$(echo $status | awk '{print $7}')
echo "binlog_file: ${binlog_file}"
echo "binlog_pos: ${binlog_pos}"

setup_replication_query="call mysql.rds_set_external_master('${SOURCE_HOST}',3306,'${SOURCE_USER}','${SOURCE_PASS}','${binlog_file}',${binlog_pos},0);"

# make sure replication is not active
mysql -h"${TARGET_HOST}" -u"${TARGET_USER}" -p"${TARGET_PASS}" -e "CALL mysql.rds_stop_replication;" || true

# set replication config
mysql -h"${TARGET_HOST}" -u"${TARGET_USER}" -p"${TARGET_PASS}" -e "${setup_replication_query}"

echo done