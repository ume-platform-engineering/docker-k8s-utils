#------------------------------------------------------------------
# Project build information
#------------------------------------------------------------------
PROJNAME := k8s-utils
IMAGE := $(PROJNAME):latest

HARBOR_REPO := harbor.platform.pe.core.underwriteme.co.uk/public
HARBOR_USERNAME := "robot\$$public+gitlab-ci"
HARBOR_PASSWORD_PUBLIC ?="unknown"

build:
	docker build -t $(IMAGE) .

scan:
	trivy --version
    ## '--light' option is deprecated and will be removed. See also: https://github.com/aquasecurity/trivy/discussions/1649
    ## cache cleanup is needed when scanning images with the same tags, it does not remove the database
	time trivy image --clear-cache
    ## update vulnerabilities db
	time trivy image --download-db-only
    ## Builds report and puts it in the default workdir $CI_PROJECT_DIR, so `artifacts:` can take it from there
    # time trivy image --ignore-policy trivy-ignore.rego --exit-code 0 --format template --template "@\contrib\html.tpl" --output $(CI_PROJECT_DIR)\trivy-scanning-report.html $(IMAGE)
    # Prints full report
	trivy image --ignore-policy trivy-ignore.rego -s "UNKNOWN,MEDIUM,HIGH,CRITICAL" --exit-code 1 $(IMAGE)

push:
	docker login -u $(HARBOR_USERNAME) -p $(HARBOR_PASSWORD_PUBLIC) harbor.platform.pe.core.underwriteme.co.uk
	docker tag $(IMAGE) $(HARBOR_REPO)/$(IMAGE)
	docker push $(HARBOR_REPO)/$(IMAGE)
	docker rmi $(HARBOR_REPO)/$(IMAGE)
	docker logout

build-and-scan: build scan

build-and-publish: build push
